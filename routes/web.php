<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detail/{id}', 'HomeController@view');

Route::post('/registnew', 'PreUserController@store'); //untuk pendaftaran user baru
Route::get('/kategori/{id}', 'HomeController@viewKategori'); //menampilkan data buku berdasarkan kategori

//tambah ke keranjang peminjaman
Route::get('/tambahKeranjang/{id}', 'PeminjamanController@tambahKeranjang');
Route::get('/keranjang', 'PeminjamanController@index');

//hapus data di keranjang
Route::delete('/keranjang/{id}', 'PeminjamanController@hapusKeranjang');

//hitung keranjang
Route::get('/hitungKeranjang', 'PeminjamanController@hitungKeranjang');

//checkout peminjaman
Route::get('/pinjam', 'PeminjamanController@pinjam');

//data peminjaman saya
Route::get('/peminjaman', 'PeminjamanController@myBook');

//Grup Route dengan awalan admin
Route::prefix('admin')->group(function() {
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'Auth\AdminController@index')->name('admin.dashboard');

    //data kategori
    Route::get('/kategori/create', 'KategoriController@create');
    Route::get('/kategori', 'KategoriController@index');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/edit/{id}', 'KategoriController@edit');
    Route::put('/kategori/edit/{id}', 'KategoriController@update');

    //data buku
    Route::get('/book', 'BookController@index');
    Route::get('/book/create', 'BookController@create');
    Route::post('/book','BookController@store');
    Route::get('/book/edit/{id}', 'BookController@edit');
    Route::put('/book/edit/{id}', 'BookController@update');
    Route::delete('/book/{id}', 'BookController@destroy');

    //pengajuan anggota
    Route::get('/pengajuan-anggota', 'PreUserController@index');
    Route::post('/approve', 'PreUserController@approve');
    Route::get('/reject/{id}', 'PreUserController@reject');
    Route::get('/hitung-preuser', 'PreUserController@hitung');

    //data anggota
    Route::get('/data-anggota', 'UserController@index');

    //pengajuan peminjaman
    Route::get('/pengajuan-peminjaman', 'PeminjamanAdminController@pengajuan');
    Route::post('/approve-peminjaman', 'PeminjamanAdminController@approve');
    Route::get('/reject-peminjaman/{id}', 'PeminjamanAdminController@reject');
    Route::get('/hitung-peminjaman', 'PeminjamanAdminController@hitung');

    //data peminjaman
    Route::get('/peminjaman', 'PeminjamanAdminController@index');
    Route::post('/kembalikan/{id}/{idBuku}', 'PeminjamanAdminController@kembalikan');
   }) ;