<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PreUserController extends Controller
{



    public function store(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $query = DB::table('pre_users')->insert([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect('/register')->with('success','Akun anda telah didaftarkan dan menunggu persetujuan admin.');
    }

    public function index(){
        $preUser = DB::table('pre_users')->get();
        return view('admin.preUsers', compact('preUser'));
    }

    public function approve(Request $request){
        //input ke tabel user
        $query = DB::table('users')->insert([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => $request['password'],
        ]);

        //hapus data di table pre_users
        DB::table('pre_users')->where('id', '=', $request['id'])->delete();

        return redirect('/admin/pengajuan-anggota');
    }

    public function reject($id){
        $query = DB::table('pre_users')->where('id', $id)->delete(); //menghapus data pengajuan anggota
        return redirect('/admin/pengajuan-anggota');
    }

    public function hitung(){
        $hitungUser = DB::table('pre_users')
        ->count();

        return response()->json(
            [
                'success' => true,
                'message' => $hitungUser,
            ]
        );
    }
}
