<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class PeminjamanAdminController extends Controller
{
    public function index(){
        $peminjaman = DB::table('peminjamans')
                ->where([
                    ['status', '!=', '0'], //tidak dalam keranjang
                ])
                ->join('books','peminjamans.books_id', 'books.id')
                ->join('users', 'peminjamans.users_id', 'users.id')
                ->select('peminjamans.*','books.tahun','books.judul', 'users.name')
                ->get();
        return view ('admin.peminjaman', compact('peminjaman')); 
    }
    public function pengajuan (){
        $pengajuan = DB::table('peminjamans')
                ->where([
                    ['status', '=', '1'], //status 1 sudah diajukan
                ])
                ->join('books','peminjamans.books_id', 'books.id')
                ->join('users', 'peminjamans.users_id', 'users.id')
                ->select('peminjamans.*','books.stock','books.judul', 'users.name')
                ->get();
        return view ('admin.pengajuanPeminjaman', compact('pengajuan'));
    }

    public function approve(Request $request){
        $id=$request['id'];
        $peminjaman = DB::table('peminjamans')
        ->where([
            ['id', $id], //mencari data peminjaman berdasarkan id
        ])
        ->first();
        //$idPeminjaman = $peminjaman->id;
        $idBuku = $peminjaman->books_id;
        //$idUser=$peminjaman->users_id;

        //update status di table peminjamans
        $update = DB::table('peminjamans')
        ->where('id','=', $id)
        ->update(['status' => 2]); //status 2 berarti sudah disetujui

        //select data buku
        $books = DB::table('books')
        ->where('id','=', $idBuku)
        ->first();
        $newStock = $books->stock-1;

        //update stock buku
        $kurangiStock= DB::table('books')
        ->where('id', $idBuku)
        ->update(['stock' => $newStock]);

        return redirect ('/admin/pengajuan-peminjaman')
        ->with (['success' => 'Peminjaman berhasil disetujui.']);
    }

    public function reject($id){
        $reject = DB::table('peminjamans')
        ->where('id', $id)
        ->update(['status' => 3]); //status 3 berarti ditolak.

        return redirect ('/admin/pengajuan-peminjaman')
        ->with (['error' => 'Peminjaman telah ditolak.']);
    }

    public function hitung (){
        $hitung = DB::table('peminjamans')
        ->where('status','=',1)
        ->count();

        return response()->json(
            [
                'success' => true,
                'message' => $hitung,
            ]
        );
    }

    public function kembalikan (Request $request){
        $id = $request['id'];
        $idBuku = $request['idBuku'];

        $book = DB::table('books')
                ->where('id','=', $idBuku)
                ->first();
        
        $stock = $book->stock;
        $tambahStock = $stock + 1;

        $kembalikan = DB::table('peminjamans')
                    ->where('id', '=', $id)
                    ->update(['status' => 4]); //status 4 berarti sudah dikembalikan

        $updateStock = DB::table('books')
                    ->where('id', '=', $idBuku)
                    ->update(['stock' => $tambahStock]); //mengembalikan stock buku
        return redirect ('/admin/peminjaman')
        ->with([
            'success' => 'Buku berhasil dikembalikan'
        ]);
        
    }
}
