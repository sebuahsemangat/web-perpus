<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    //untuk menampilkan data kategori
    public function index(){
        $kategori = DB::table('labels')->get();
        return view('admin.kategori', compact('kategori'));
    }
    //Controller untuk menampilkan form tambah kategori
    public function create() {
        return view ('admin.add_kategori');
    }

    //view edit kategori
    public function edit($id) {
        $kategori = DB::table('labels')
        ->where ('id', '=', $id)
        ->first();
        return view ('admin.editKategori', compact('kategori'));
    }

    //proses edit kategori
    public function update($id, Request $request){
        $request->validate([
            'label' => 'required|unique:labels',
        ]);
        
        DB::table('labels')
        ->where('id', '=', $id)
        ->update([
            "label" => $request["label"],
        ]);

        return redirect ('/admin/kategori');
    }

    //Untuk Insert Data Kategori
    public function store(Request $request){
        $request->validate([
            'label' => 'required|unique:labels',
        ]);
        $query = DB::table('labels')->insert([
            "label" => $request["label"],
        ]);
        return redirect('/admin/kategori')->with('success','Data kategori berhasil ditambahkan');
    }
}
