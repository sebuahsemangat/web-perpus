<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class PeminjamanController extends Controller
{
    
    public function tambahKeranjang($id)
    {
        //mencegah user meminjam buku yang sama
        $hitung = DB::table('peminjamans')
        ->where('books_id','=',$id)
        ->where('users_id','=', Auth::id())
        ->count();
        if ($hitung>0) {
            return response()->json(
                [
                    'success' => true,
                    'message' => 'Buku ini sudah ada di keranjang atau sedang kamu pinjam!'
                ]
            );
        } else {
            $peminjaman = DB::table('peminjamans')->insert([
                'books_id' => $id, 
                'users_id' => Auth::id(), 
                'tanggal' => date('Y-m-d H:i:s'), 
                'status' => 0, //status 0 berarti baru masuk ke keranjang
            ]);
            return response()->json(
                [
                    'success' => true,
                    'message' => 'Buku berhasil ditambahkan ke keranjang peminjaman.'
                ]
            );
        }
        

    }

    public function index(){
        $userId = Auth::id();
        if ($userId==''){
            return redirect ('/login');
        }
        else {
            $keranjang = DB::table('peminjamans')
                ->where([
                    ['users_id', '=', $userId],
                    ['status', '=', '0'], //status masih dalam keranjang belum diajukan
                ])
                ->join('books','peminjamans.books_id', 'books.id')
                ->select('peminjamans.*','books.*')
                ->get();
            
            $hitung = DB::table('peminjamans')
                ->where([
                    ['users_id', '=', $userId],
                    ['status', '=', '0'], //status masih dalam keranjang belum diajukan
                ])
                ->count(); //menghitung jumlah data di keranjang
                
                return view ('keranjang', compact('keranjang','hitung'));
                
        }
            
    }

    public function pinjam(Request $request) {
        $peminjaman = DB::table('peminjamans')
        ->where([
            ['users_id', '=', Auth::id()],
            ['status', '=', '0'], //status masih dalam keranjang belum diajukan
        ])
        ->update(['status' => 1]); //status 1 berarti sudah diajukan
        return redirect('/keranjang')->with([
            'success' => 'Peminjaman berhasil diajukan. Silahkan menunggu persetujuan admin.'
        ]);
    }

    public function hapusKeranjang($id){
        $keranjang = DB::table('peminjamans')
        ->where([
            ['users_id', '=', Auth::id()],
            ['books_id', '=', $id]
            ])
        ->delete();

        return response()->json([
            'success' => 'Buku dihapus dari keranjang.'
        ]);
    }

    public function myBook() {
        $userId = Auth::id();
        if ($userId==''){
            return redirect ('/login');
        }
        else {
            $peminjaman = DB::table('peminjamans')
            ->where([
                ['users_id', '=', $userId],
                ['status', '!=', 0] //status bukan di keranjang
                ])
            ->join('books','peminjamans.books_id', 'books.id')
            ->select('peminjamans.*','books.*')
            ->get();
            return view ('myBook', compact('peminjaman'));
        }
    }

    public function hitungKeranjang(){
        $userId = Auth::id();
        $hitungKeranjang = DB::table('peminjamans')
                            ->where([
                                ['users_id', '=', $userId],
                                ['status', '=', 0] 
                            ])
                            ->count();
                            return response()->json(
                                [
                                    'success' => true,
                                    'hitung' => $hitungKeranjang
                                ]
                            );
    }
}
