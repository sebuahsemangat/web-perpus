<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use App\Http\Controllers\Controller;
use DB;
class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(){
        //menampilkan data buku di halaman admin
        $book = DB::table('books')
        ->join('labels','books.labels_id', 'labels.id')
        ->select('books.*','labels.label')
        ->orderBy('id', 'desc')->get();
        return view ('admin.book', compact('book'));
    }
    
     public function create(){
        //menampilkan kategori di halaman tambah buku
        $kategori = DB::table('labels')->get();
        return view ('admin.add_book', compact('kategori'));
    }

    public function store(Request $request){
        //Menyimpan data buku

        //validasi gambar
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
        //rename gambar
        $imageName = time().'.'.$request->image->extension();  
        
        //pindahkan gambar ke folder
        $request->image->move(public_path('book_images'), $imageName);
        
        //insert data ke database
        $query = DB::table('books')->insert([
            "judul" => $request["judul"],
            "penulis" => $request["penulis"],
            "penerbit" => $request["penerbit"],
            "tahun" => $request["tahun"],
            "deskripsi" => $request["deskripsi"],
            "gambar" => $imageName,
            "labels_id" => $request["label"],
            "stock" => $request["stock"],
            "rak" => $request["rak"],
        ]);
        return redirect('/admin/book/create')->with('success','Data buku berhasil ditambahkan');
    }

    public function edit ($id){
        $buku = DB::table('books')
        ->where('id','=', $id)
        ->first();
        $kategori = DB::table('labels')->get();
        return view ('admin.editBook', compact('buku', 'kategori'));
    }

    public function update ($id, Request $request){
        $buku = DB::table('books')
        ->where('id','=', $id)
        ->first();
        
        $request->validate([
            'image' => '|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if ($request['image']=='') { //jika gambar tidak diganti
            $imageName = $buku->gambar;
        } else {
            //rename gambar
            $imageName = time().'.'.$request->image->extension(); 
            
            //pindahkan gambar ke folder
            $request->image->move(public_path('book_images'), $imageName);
            File::delete('book_images/'.$buku->gambar); //hapus file gambar lama
        }
        
        $update = DB::table('books')
              ->where('id', $id)
              ->update([
                "judul" => $request["judul"],
                "penulis" => $request["penulis"],
                "penerbit" => $request["penerbit"],
                "tahun" => $request["tahun"],
                "deskripsi" => $request["deskripsi"],
                "gambar" => $imageName,
                "labels_id" => $request["label"],
                "stock" => $request["stock"],
                "rak" => $request["rak"],
                ]);
        
        return redirect ('/admin/book')->with([
            'success' => 'Data buku berhasil diubah.',
        ]);
    }

    public function destroy ($id) {
        $buku = DB::table('books')
        ->where('id','=', $id)
        ->first();

        File::delete('book_images/'.$buku->gambar);

        $buku = DB::table('books')
        ->where('id','=', $id)
        ->delete();
        return redirect ('/admin/book')->with([
            'success' => 'Data buku berhasil dihapus.',
        ]);  
    }
}
