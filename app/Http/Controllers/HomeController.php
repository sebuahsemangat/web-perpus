<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index(){

        //menampilkan kategori di halaman home
        $kategori = DB::table('labels')->get();

        //menampilkan data buku di halaman home
        $book = DB::table('books')
                        ->orderBy('id','desc') //diurutkan dari yang terbaru diinput
                        ->paginate(15); //limit menampilkan 15 data buku
        return view('welcome', compact('kategori','book')); 
    }

    public function view($id){
        $book = DB::table('books')
        ->where('id', $id) //menampilkan buku berdasarkan id 
        ->first(); //menampilkan satu data buku
        
        $label = DB::table('labels')
        ->where('id', $book->labels_id) //menampilkan kategori berdasarkan labels_id di tabel books
        ->first();
        return view ('detailBuku', compact('book','label'));
    }

    public function viewKategori($id){

        $kategori = DB::table('labels')->get();

        $book = DB::table('books')
        ->where ('labels_id', $id) //kondisi berdasarkan kategori
        ->paginate(15); //limit 15
        return view ('viewKategori', compact('book','kategori'));
    }
}
