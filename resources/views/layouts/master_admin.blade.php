<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin E-Perpus</title>

    <!-- Custom fonts for this template -->
    <link href="{{asset('/theme/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('/theme/css/sbadmin.css')}}" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="{{asset('/theme/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/" target="_blank">
                
                <div class="sidebar-brand-text mx-3">Admin E-Perpus</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="/admin">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <li class="nav-item">
                <a class="nav-link" href="/admin/kategori">
                    <i class="fas fa-tags"></i>
                    <span>Data Kategori</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/admin/book">
                    <i class="fas fa-book"></i>
                    <span>Data Buku</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-user"></i>
                    <span>Anggota</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="/admin/pengajuan-anggota">Pengajuan Anggota</a>
                        <a class="collapse-item" href="/admin/data-anggota">Data Anggota</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-table"></i>
                    <span>Peminjaman</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="/admin/pengajuan-peminjaman">Pengajuan Peminjaman</a>
                        <a class="collapse-item" href="/admin/peminjaman">Data Peminjaman</a>
                    </div>
                </div>
            </li>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <form class="form-inline">
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/pengajuan-anggota"
                                aria-haspopup="true" aria-expanded="false"
                                title="Pengajuan User Baru">
                                <i class="fas fa-user fa-fw"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger badge-counter" id="notif_user"></span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/admin/pengajuan-peminjaman"
                                aria-haspopup="true" aria-expanded="false"
                                title="Pengajuan Peminjaman Baru">
                                <i class="fas fa-table fa-fw"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger badge-counter" id="notif_peminjaman"></span>
                            </a>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                
                                <img class="img-profile rounded-circle"
                                    src="{{asset('/img/undraw_profile.svg')}}">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/admin/logout">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">E-Perpus Admin Page</h6>
                        </div>
                        <div class="card-body">
                            @yield('content')
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; E-Perpus | Developed by SebuahSemangat</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('/theme/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/theme/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('/theme/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('/theme/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{asset('/theme/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/theme/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('/theme/js/demo/datatables-demo.js')}}"></script>
        <script type="text/javascript">
            //Munculkan notifikasi pengajuan peminjaman

            setInterval(function hitungPeminjaman()  {
                
                $.ajax({
                    url:'/admin/hitung-peminjaman',
                    type: 'GET',
                    data: {
                        _token : $("input[name=_token]").val()
                    },
                    success:function(response){
                        if(response.success){
                            document.getElementById("notif_peminjaman").innerHTML = response.message;
                        }else{
                            alert("Error")
                        }

                    }
                });
                
            }, 1000);
    
        </script>

        <script type="text/javascript">
            //Munculkan notifikasi pengajuan user baru

            setInterval(function hitungUser()  {
                
                $.ajax({
                    url:'/admin/hitung-preuser',
                    type: 'GET',
                    data: {
                        _token : $("input[name=_token]").val()
                    },
                    success:function(response){
                        if(response.success){
                            document.getElementById("notif_user").innerHTML = response.message;
                        }else{
                            alert("Error")
                        }

                    }
                });
                
            }, 1000);

        </script>
</body>

</html>