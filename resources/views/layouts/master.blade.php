<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>E-Perpus</title>
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{asset('/theme/css/style.css')}}" rel="stylesheet">

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <!-- Topbar Start -->
    <div class="container-fluid">
        
        <div class="row align-items-center py-3 px-xl-5">
            <div class="col-lg-3 d-none d-lg-block">
                <a href="/" class="text-decoration-none">
                    <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">E</span>Perpus</h1>
                </a>
            </div>
            <div class="col-lg-6 col-6 text-left">
                <form action="">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Cari buku di sini">
                        <div class="input-group-append">
                            <span class="input-group-text bg-transparent text-primary">
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-3 col-6 text-right">
            <!-- Authentication Links -->
        @guest
            <a href="{{ route('login') }}" class="btn border">
                <i class="fas fa-user text-primary"></i>
                <span class="badge">Login</span>
            </a>
            @if (Route::has('register'))
            <a href="{{ route('register') }}" class="btn border">
                <i class="far fa-user text-primary"></i>
                <span class="badge">Register</span>
            </a>
            @endif
            @else
            <a href="/peminjaman" class="btn border">
                <i class="fas fa-table text-primary"></i>
                
            </a>
            <a href="/keranjang" class="btn border">
                <i class="fas fa-shopping-cart text-primary"></i>
                <span class="badge" id="cart_counter"></span>
            </a>
            <a href="{{ route('logout') }}" class="btn border"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt text-primary"></i>
                <span class="badge">Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        @endguest
                
                
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Shop Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5 justify-content-center">
            @yield('sidebar')
            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-12">
                <div class="row pb-3">
                    
                    @yield('content')
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->


    <!-- Footer Start -->
    <div class="container-fluid bg-secondary text-dark mt-5 pt-5">
        <div class="row border-top border-light mx-xl-5 py-4">
            <div class="col-md-6 px-xl-0">
                <p class="mb-md-0 text-center text-md-left text-dark">
                    &copy; <a class="text-dark font-weight-semi-bold" href="#">E Perpus</a>. All Rights Reserved. Designed
                    by
                    <a class="text-dark font-weight-semi-bold" href="https://gitlab.com/sebuahsemangat">Apep Wahyudin</a>
                </p>
            </div>
            
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    
    <!-- Contact Javascript File -->
    <script src="{{asset('/theme/mail/jqBootstrapValidation.min.js')}}"></script>
    <script src="{{asset('/theme/mail/contact.js')}}"></script>

<script type="text/javascript">
function tambahKeranjang(id){
    $.ajax({
        url:'/tambahKeranjang/'+id,
        type: 'GET',
        data: {
            _token : $("input[name=_token]").val()
        },
        success:function(response){
            if(response.success){
                  alert(response.message) //Pesan dari controller
              }else{
                  alert("Error")
              }

        }
    });
}

function hapusKeranjang(id){
    if (confirm("Hapus buku ini dari keranjang?")){
        $.ajax({
            url: '/keranjang/'+id,
            type: 'DELETE',
            data: {
            _token : $("input[name=_token]").val()
        },
            success:function(response)
            {
                $("#sid"+id).remove();
                $("#cart-btn").remove();
            }
            

        });
    }
}

setInterval(function hitungKeranjang()  {
                
                $.ajax({
                    url:'/hitungKeranjang',
                    type: 'GET',
                    data: {
                        _token : $("input[name=_token]").val()
                    },
                    success:function(response){
                        if(response.success){
                            document.getElementById("cart_counter").innerHTML = response.hitung;
                        }else{
                            alert("Error")
                        }

                    }
                });
                
            }, 1000);
</script>

</body>

</html>