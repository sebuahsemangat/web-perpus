
@extends('layouts.master')
@section('content')
                
            <div class="col-lg-12 table-responsive mb-5">
            @if(session('success'))
                <p class="alert alert-success">
                    {{session('success')}}
                </p>

            @else
                <table class="table table-bordered text-center mb-0">
                    <thead class="bg-secondary text-dark">
                        <tr>
                            <th>Judul</th>
                            <th>Penulis</th>
                            <th>Penerbit</th>
                            <th>Tahun</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle">
                    <form>
                    @csrf
                        @forelse($keranjang as $key => $data)
                        <tr id="sid{{$data->id}}">
                            <td class="align-middle">
                                {{$data->judul}}
                            </td>
                            <td class="align-middle">
                                {{$data->penulis}}
                            </td>
                            <td class="align-middle">
                                {{$data->penerbit}}
                            </td>
                            <td class="align-middle">
                                {{$data->tahun}}
                            </td>
                            <td class="align-middle">
                                
                                <a href="javascript:void(0)" onclick="hapusKeranjang({{$data->id}})" class="btn btn-sm btn-primary"><i class="fa fa-times"></i></a>
                                
                            </td>
                        </tr> 
                        @empty 
                        <tr>
                            <td colspan='5'>Keranjang buku kamu masih kosong</td>
                        </tr>  
                        @endforelse
                        
                        @if($hitung <1)                              
                        
                        @else
                        <tr align="right">
                            <td colspan="5">
                            <a class="btn btn-lg btn-primary" href="/pinjam" id="cart-btn">Ajukan Peminjaman</a>
                            </td>
                        </tr>
                        @endif
                        
                        
                    </form>
                    </tbody>
                </table>
            @endif
            </div>
@endsection