
@extends('layouts.master')
@section('content')
    

<div class="col-lg-5 pb-5">
    <div id="product-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner border">
            <div class="carousel-item active">
                <img class="w-100 h-100" src="{{asset('/book_images').'/'.$book->gambar}}" alt="Image">
            </div>
            
        </div>
    </div>
</div>

<div class="col-lg-7 pb-5">
    <h3 class="font-weight-semi-bold">{{$book->judul}}</h3>
    <span class="badge bg-primary mb-3" style="color: white">
    {{$label->label}}
    </span>
    <p class="mb-4">
        {{$book->deskripsi}}
    </p>
    <div class="d-flex mb-3">
        <p class="text-dark font-weight-medium mb-0 mr-3">Penulis:</p>
        {{$book->penulis}}
    </div>
    <div class="d-flex mb-4">
        <p class="text-dark font-weight-medium mb-0 mr-3">Penerbit:</p>
        {{$book->penerbit}}
    </div>
    <div class="d-flex mb-4">
        <p class="text-dark font-weight-medium mb-0 mr-3">Tahun Terbit:</p>
        {{$book->tahun}}
    </div>
    <div class="d-flex mb-4">
        <p class="text-dark font-weight-medium mb-0 mr-3">Stock:</p>
        {{$book->stock}} Eksemplar
    </div>
    <div class="d-flex mb-4">
        <p class="text-dark font-weight-medium mb-0 mr-3">Nomor Rak:</p>
        {{$book->rak}}
    </div>
    <div class="d-flex align-items-center mb-4 pt-2">
        @if ($book->stock == 0)
        <span class="alert alert-danger">Stock kosong. Silahkan kembali saat stock sudah tersedia.</span>       
        
    @else
        @guest
        <span href="" onclick="return alert('Silahkan login untuk meminjam buku!')" class="btn border btn-primary" style="color: white">
            <i class="fas fas fa-shopping-cart" style="color: white"></i>
            <span class="badge">Pinjam</span>
        </span>
            @else
            <a href="javascript:void(0)" class="btn btn-primary" onclick="tambahKeranjang({{$book->id}})">
                        
                <i class="fas fa-sign-out-alt" style="color: white"></i>
                <span class="badge">Pinjam</span>
                
            </a>
        @endguest
    @endif
    </div>
    
</div>

@endsection