@extends('layouts.master_admin')

@section('content')
<form class="user" id="sign_in_adm" method="POST" action="/admin/kategori/edit/{{$kategori->id}}">
    @csrf
    @method('PUT')
    <div class="form-group">Nama Kategori:</div>

    @if(session('success'))
      <p class="alert alert-success">
        {{session('success')}}
      </p> 
    @endif

    @error('label')
                    <div class="alert alert-danger">
                        Kategori sudah digunakan. Silahkan masukkan kategori lainnya!
                    </div>
    @enderror
  <div class="form-group col-lg-3">
    
    <input class="form-control form-control-user" type="text" name="label" placeholder="Nama Kategori" value="{{$kategori->label}}" required autofocus>    
    
  </div>
  
  <div class="form-group col-lg-3">
    <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
  </div>
  </form>

  <hr>

@endsection