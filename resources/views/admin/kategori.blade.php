@extends('layouts.master_admin')

@section('content')

<a href="/admin/kategori/create">
    <button class="btn btn-primary mb-3">Tambah Data</button>
</a>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="50%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kategori</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
              <th>Nama Kategori</th>
              <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
          <!--awal pengulangan kategori-->
          @forelse ($kategori as $key=>$data)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$data->label}}</td>
                <td>
                    <a href="/admin/kategori/edit/{{$data->id}}" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a>
                    <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</a>
                </td>
            </tr>
            <!--akhir pengulangan kategori-->
          @empty
            <tr colspan="3">
                <td>Belum ada kategori</td>
            </tr>
          @endforelse
            
          
        </tbody>
    </table>
</div>
@endsection