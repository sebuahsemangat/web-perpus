@extends('layouts.master_admin')

@section('content')

<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="80%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
            </tr>
        </tfoot>
        <tbody>
          @forelse ($user as $key=>$dataUser)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$dataUser->name}}</td>
                <td>{{$dataUser->email}}</td>
                
            </tr>
          @empty
            <tr>
                <td  colspan="3">Data anggota masih kosong.</td>
            </tr>
          @endforelse
            
          
        </tbody>
    </table>
</div>
@endsection