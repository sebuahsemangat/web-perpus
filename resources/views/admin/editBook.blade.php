@extends('layouts.master_admin')

@section('content')

<form class="user" id="sign_in_adm" method="POST" action="/admin/book/edit/{{$buku->id}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    @if(session('success'))
      <p class="alert alert-success">
        {{session('success')}}
      </p> 
    @endif
    
    <p class="ml-3">Judul Buku:</p>
    @error('judul')
                    <div class="alert alert-danger">
                        Judul buku sudah digunakan!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control form-control-user" type="text" name="judul" placeholder="Judul Buku" value="{{ $buku->judul }}" required autofocus>
    </div>

    <p class="ml-3">Penulis:</p>
    @error('penulis')
                    <div class="alert alert-danger">
                        Nama penulis harus diisi!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control form-control-user" type="text" name="penulis" placeholder="Nama Penulis" value="{{ $buku->penulis }}" required autofocus>
    </div>

    <p class="ml-3">Penerbit:</p>
    @error('penerbit')
                    <div class="alert alert-danger">
                        Nama penerbit harus diisi!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control form-control-user" type="text" name="penerbit" placeholder="Penerbit" value="{{ $buku->penerbit }}" required autofocus>
    </div>

    <p class="ml-3">Tahun:</p>
    @error('tahun')
                    <div class="alert alert-danger">
                        Tahun harus diisi!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control form-control-user" type="number" name="tahun" placeholder="Tahun terbit" value="{{ $buku->tahun }}" required>
    </div>

    <p class="ml-3">Deskripsi:</p>
    @error('deskripsi')
                    <div class="alert alert-danger">
                        Deskripsi harus diisi!
                    </div>
    @enderror
    <div class="form-group col-lg-7">
        <textarea class="form-control" name="deskripsi" value="{{ old('deskripsi') }}" required>{{$buku->deskripsi}}</textarea>
    </div>

    <p class="ml-3">Gambar:</p>
    @error('image')
                    <div class="alert alert-danger">
                        Pilih file dengan format JPG, JPEG, atau PNG maksimal 2MB!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control-user" type="file" name="image" value="">
    </div>

    <p class="ml-3">Kategori:</p>
    
    <div class="form-group col-lg-5">
        <select class="form-control" name="label" value="" required>
            @foreach ($kategori as $item=>$data)
            <option value="{{$data->id}}">{{$data->label}}</option>
            @endforeach
            
        </select>
    </div>

    <p class="ml-3">Stock Buku:</p>
    @error('tahun')
                    <div class="alert alert-danger">
                        Stock harus diisi!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control form-control-user" type="number" name="stock" placeholder="Stock Buku" value="{{$buku->stock}}" required>
    </div>

    <p class="ml-3">Nomor Rak:</p>
    @error('rak')
                    <div class="alert alert-danger">
                        Nomor rak harus diisi!
                    </div>
    @enderror
    <div class="form-group col-lg-5">
        <input class="form-control form-control-user" type="text" name="rak" placeholder="Nomor Rak" value="{{ $buku->rak }}" required>
    </div>
  
    <div class="form-group col-lg-3">
        <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
    </div>
  </form>

  <hr>

@endsection