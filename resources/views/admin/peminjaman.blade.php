@extends('layouts.master_admin')

@section('content')
@if(session('success'))
                <p class="alert alert-success">
                    {{session('success')}}
                </p>
@endif
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th width="70px">No</th>
                <th>Nama Anggota</th>
                <th>Judul Buku</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Anggota</th>
                <th>Judul Buku</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
          @forelse ($peminjaman as $key=>$data)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$data->name}}</td>
                <td>{{$data->judul}}</td>
                <td>{{$data->tanggal}}</td>
                <td>
                    @switch($data->status)
                                    @case(1)
                                        {{'Sedang Ditinjau'}}
                                        @break
                                    @case(2)
                                        {{'Disetujui'}}
                                        @break
                                    @case(3)
                                        {{'Ditolak'}}
                                    @break
                                    @case(4)
                                    {{'Sudah Dikembalikan'}}
                                    @break
                                    @default
                                        {{'Pengajuan'}}
                                @endswitch
                </td>
                <td align="center">
                    @if ($data->status==2)
                        <form onsubmit="return confirm ('Kembalikan buku ini?')" action="/admin/kembalikan/{{$data->id}}/{{$data->books_id}}" method="post">
                            @csrf
                            <button title="Kembalikan" type="submit" class="btn btn-success">
                            <i class="fas fa-undo"></i>
                            </button>
                         </form>
                    @else
                        None
                    @endif
                    
                </td>
            </tr>
            <!--akhir pengulangan kategori-->
          @empty
            <tr>
                <td  colspan="3">Data peminjaman masih kosong.</td>
            </tr>
          @endforelse
            
          
        </tbody>
    </table>
</div>
@endsection