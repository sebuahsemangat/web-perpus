@extends('layouts.master_admin')

@section('content')

<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="80%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
          @forelse ($preUser as $key=>$dataUser)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$dataUser->name}}</td>
                <td>{{$dataUser->email}}</td>
                <td>
                    <form method="POST" action="/admin/approve" class="form-inline" onsubmit="return confirm('Approve anggota ini?')">
                        @csrf
                        <input type="hidden" value="{{$dataUser->id}}" name="id">
                        <input type="hidden" value="{{$dataUser->name}}" name="name">
                        <input type="hidden" value="{{$dataUser->email}}" name="email">
                        <input type="hidden" value="{{$dataUser->password}}" name="password">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Approve</button>
                    
                        <a onclick="return confirm('Reject anggota ini?')" href="/admin/reject/{{$dataUser->id}}" class="btn btn-danger ml-1"><i class="fas fa-times"></i> Reject</a>
                    </form>
                    
                </td>
            </tr>
          @empty
            <tr>
                <td  colspan="3">Belum ada pengajuan anggota baru.</td>
            </tr>
          @endforelse
            
          
        </tbody>
    </table>
</div>
@endsection