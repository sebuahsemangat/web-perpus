@extends('layouts.master_admin')

@section('content')
@if (session('success'))
<p class="alert alert-success">
    {{session('success')}}
</p>
@elseif(session('error'))
<p class="alert alert-danger">
    {{session('error')}}
@endif
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th width="70px">No</th>
                <th>Nama Anggota</th>
                <th>Judul Buku</th>
                <th>Stock</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Anggota</th>
                <th>Judul Buku</th>
                <th>Stock</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
          @forelse ($pengajuan as $key=>$pengajuans)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$pengajuans->name}}</td>
                <td>{{$pengajuans->judul}}</td>
                <td>{{$pengajuans->stock}}</td>
                <td>
                    <form method="POST" action="/admin/approve-peminjaman" class="form-inline" onsubmit="return confirm('Approve peminjaman ini?')">
                        @csrf
                        @if ($pengajuans->stock<1)
                        <span class="btn btn-primary disabled" onclick="alert('Stock buku ini sedang kosong!')"><i class="fas fa-check"></i></span> 
                        @else
                        <input type="hidden" value="{{$pengajuans->id}}" name="id">
                        <input type="hidden" value="{{$pengajuans->stock}}" name="stock">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i></button> 
                        @endif
                        
                        <a onclick="return confirm('Tolak peminjaman ini?')" href="/admin/reject-peminjaman/{{$pengajuans->id}}" class="btn btn-danger ml-1"><i class="fas fa-times"></i></a>
                    </form>
                    
                </td>
            </tr>
            <!--akhir pengulangan kategori-->
          @empty
            <tr>
                <td  colspan="3">Belum ada pengajuan peminjaman baru.</td>
            </tr>
          @endforelse
            
          
        </tbody>
    </table>
</div>
@endsection