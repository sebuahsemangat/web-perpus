@extends('layouts.master_admin')

@section('content')

<a href="/admin/book/create">
    <button class="btn btn-primary mb-3">Tambah Data</button>
</a>

@if (session('success'))
    <p class="alert alert-success">
        {{session('success')}}
    </p>
@endif
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width:30px">No</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Penulis</th>
                <th>Penerbit</th>
                <th>Tahun</th>
                <th>Stock</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Penulis</th>
                <th>Penerbit</th>
                <th>Tahun</th>
                <th>Stock</th>
                <th style="width: 70px">Action</th>
            </tr>
        </tfoot>
        <tbody>
          <!--awal pengulangan kategori-->
          @forelse ($book as $key=>$data)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$data->judul}}</td>
                <td>{{$data->label}}</td>
                <td>{{$data->penulis}}</td>
                <td>{{$data->penerbit}}</td>
                <td>{{$data->tahun}}</td>
                <td>{{$data->stock}}</td>
                <td align="center">
                    <form action="/admin/book/{{$data->id}}" method="post" onsubmit="return confirm('Hapus data buku ini?')">
                        @csrf
                        @method('DELETE')
                    <a href="/admin/book/edit/{{$data->id}}" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            <!--akhir pengulangan kategori-->
          @empty
            <tr colspan="3">
                <td>Belum ada data buku</td>
            </tr>
          @endforelse
            
          
        </tbody>
    </table>
</div>
@endsection