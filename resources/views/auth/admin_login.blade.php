<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Perpus | Login</title>

    
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('/theme/css/sbadmin.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Login Admin E-Perpus</h1>
                                    </div>
                                    @if (session('errors'))
                                        <p class="alert alert-danger">{{'E-Mail atau Password salah!'}}</p>
                                    @endif
                                    <form class="user" id="sign_in_adm" method="POST" action="{{ route('admin.login.submit') }}">
                                      {{ csrf_field() }}
                                    <div class="form-group">
                                      <input class="form-control form-control-user" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>
                                    </div>

                                                                      
                                    <div class="form-group">
                                      <input class="form-control form-control-user" type="password" name="password" placeholder="Password" required>
                                    </div>
                                    
                                    
                                      <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
                                    
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                      <a class="small" href="/">Back to Home</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>


</body>

</html>