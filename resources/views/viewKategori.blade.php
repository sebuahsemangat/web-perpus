@extends('layouts.master')
@section('sidebar')
    <!-- Shop Sidebar Start -->
<div class="col-lg-3 col-md-12">
    <div class="mb-4 pb-4">
        <h5 class="font-weight-semi-bold mb-4">Kategori Buku</h5>
        <nav class="navbar navbar-vertical navbar-light align-items-start p-0 border border-top-0 border-bottom-0 bg-light" id="navbar-vertical" style="width: calc(100% - 30px); z-index: 1;">
            <div class="navbar-nav w-100" style="height: 410px">
                
                @forelse ($kategori as $item=>$dataKategori)
                <a href="/kategori/{{$dataKategori->id}}" class="nav-item nav-link">{{$dataKategori->label}}</a>
                @empty
                Belum ada data
                @endforelse
                
            </div>
        </nav>
    </div>
    
</div>
<!-- Shop Sidebar End -->
@endsection


@section('content')
    <!--start home-->
    

    @forelse ($book as $item=>$dataBuku)
    <div class="col-lg-4 col-md-6 col-sm-12 pb-1">
        <div class="card product-item border-0 mb-4">
            <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                <img class="img-fluid w-100" src="{{asset('/book_images').'/'.$dataBuku->gambar}}" alt="">
            </div>
            <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                <h5 class="mb-3">{{$dataBuku->judul}}</h5>
                <div class="d-flex justify-content-center">
                    <h6>{{$dataBuku->penulis}}</h6>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-between bg-light border">
                <a href="/detail/{{$dataBuku->id}}" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>
                    Lihat Deskripsi
                </a>

                @if ($dataBuku->stock == 0)
                
                    Stock kosong
                @else
                    @guest
                    <span href="" onclick="return alert('Silahkan login untuk meminjam buku!')" class="btn border btn-primary" style="color: white">
                        <i class="fas fas fa-shopping-cart" style="color: white"></i>
                        <span class="badge">Pinjam</span>
                    </span>
                    @else
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="tambahKeranjang({{$dataBuku->id}})">
                            
                            <i class="fas fa-sign-out-alt" style="color: white"></i>
                            <span class="badge">Pinjam</span>
                            
                        </a>
                    @endguest
                @endif
                
            </div>
        </div>
    </div>
    @empty
    <p class="alert alert-warning" style="width: 100%">Oops! Belum ada data buku di kategori ini :(
        <br>Silahkan pilih kategori lainnya ;)
    </p>
    @endforelse
    
    
    <div class="col-12 pb-1 justify-content-center mb-3">
        <div class="d-flex justify-content-center">{{ $book->links() }}</div>
    </div>
    <!--end home-->
@endsection
                    