
@extends('layouts.master')
@section('content')
                
            <div class="col-lg-12 table-responsive mb-5">
            <h4 align='center'>Riwayat Peminjaman Buku</h4>
            <br>
                <table class="table table-bordered text-center mb-0">
                    <thead class="bg-secondary text-dark">
                        <tr>
                            <th>Judul</th>
                            <th>Penulis</th>
                            <th>Penerbit</th>
                            <th>Tahun</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle">
                
                    @csrf
                        @forelse($peminjaman as $key => $data)
                        <tr id="sid{{$data->id}}">
                            <td class="align-middle">
                                {{$data->judul}}
                            </td>
                            <td class="align-middle">
                                {{$data->penulis}}
                            </td>
                            <td class="align-middle">
                                {{$data->penerbit}}
                            </td>
                            <td class="align-middle">
                                {{$data->tahun}}
                            </td>
                            <td class="align-middle">
                                @switch($data->status)
                                    @case(1)
                                        {{'Sedang Ditinjau'}}
                                        @break
                                    @case(2)
                                        {{'Disetujui'}}
                                        @break
                                    @case(3)
                                        {{'Ditolak'}}
                                    @break
                                    @case(4)
                                    {{'Sudah Dikembalikan'}}
                                    @break
                                    @default
                                        {{'Pengajuan'}}
                                @endswitch
                                
                            </td>
                        </tr> 
                        @empty 
                        <tr>
                            <td colspan='5'>Kamu belum pernah meminjam buku.</td>
                        </tr>  
                        @endforelse
                        
                    </tbody>
                </table>
        
            </div>
@endsection